<!DOCTYPE html>
<html lang="en" dir="ltr">

  <head>
    <meta charset="utf-8">
    <title>PLANILLA 2018</title>
    <link rel="stylesheet" href="icon/style.css">
    <link rel="stylesheet" href="css/registro.css">
  </head>

  <body>
    <header>
      <span class="lnr lnr-menu show"></span>
    </header>

      <main>

        <div class="content-menu">
          <li><span class="lnr lnr-home icon1" href="registro.php"></span><h4 class="text1">Inicio</h4></li>
          <li><span class="lnr lnr-user icon2"></span><h4 class="text2">Empleado</h4></li>
          <li><a href="registro.php"><span class="lnr lnr-pencil icon3"></span></a><h4 class="text3">Registro</h4></a></li>
          <li><span class="lnr lnr-briefcase icon4"></span><h4 class="text4">Nomina</h4></li>

          <li><span class="lnr lnr-users icon5"></span><h4 class="text5">Nuevo Usuario</h4></li>
          <li><span class="lnr  lnr-code icon6"></span><h4 class="text6">Senting</h4></li>
          <li></li>
          <li></li>

        </div>
        <article>
          <div class="content-formulario">
            <h2 class="h2-registro">Registro de Empleado</h2>
            <form class="form-1" method="post">

              <div class="input-registro">
                <div class="span8">
                  <input type="text" name="" value="" placeholder="Codigo" required>
                  <input type="text" name="" value="" placeholder="Nombre" required>
                  <input type="text" name="" value="" placeholder="Apellido" required>

                   <select class="" name="">
                     <option value="">Departamentos</option>
                     <option value="">Administracion</option>
                     <option value="">Contabilidad</option>
                     <option value="">Informatica</option>
                   </select>

                   <input type="text" name="" value="" placeholder="Pusto">
                   <input type="text" name="" value="" placeholder="Comision" required>
                   <input type="text" name="" value="" placeholder="Salario">
                   <input type="text" name="" value="" placeholder="IR">
                  <input class="fecha" type="text" name="" placeholder="Fecha de Ingreso">
                  <input type="text" name="" value="">
                  <input type="text" name="" value="">
                  <input type="text" name="" value="">
                  <input type="text" name="" value="">
                  <input type="text" name="" value="">
                  <input id="button" class="button" type="submit" name="button" value="Guardar">

                </div>



              </div>



            </form>
            <div class="tabla">
              <table>
                <thead>
                  <tr>
                    <th>Codigo</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Departamentos</th>
                    <th>Puesto</th>
                    <th>Comision</th>
                    <th>Salario</th>
                    <th>IR</th>
                    <th>Fecha de Ingreso</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                  <td>0005</td>
                  <td>Abraham</td>
                  <td>Gutierrez</td>
                  <td>Informatica</td>
                  <td>200</td>
                  <td>Deceloper WEB</td>
                  <td>22000</td>
                  <td>%5</td>
                  <td>02-05-2016</td>
                </tr>

                </tbody>
              </table>
            </div>
          </div>

        </article>

      </main>






  </body>
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <script src="js/script.js"></script>
</html>
